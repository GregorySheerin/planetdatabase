﻿const base = "https://planetdatabase.s3-eu-west-1.amazonaws.com/Images/"; //base url for aws s3 bucket
const select = document.getElementById("planetSelect"); //ref to the planet select
const planet = document.getElementById("planetImg"); //ref to the plance image

document.getElementById("planetSelect").addEventListener('change', () => {//whenever the select has a value change
  var selectedVal = select.options[select.selectedIndex].value; //get the selected planet from the list
  planet.src = base + selectedVal + ".jpg"; //set the Src to the base url + the planet name
  $.ajax({ //ajax call to get the planet's distance 
    url: "/Home/DisplayDistanceFromSun/",//call the controller,
    data: {
      name: selectedVal //send it the selected planet
    },
    success: function (data) { //on sucess
      $("#distFromSun").html(data); //render the partial view in the div
    },
    error: function () { //on error
      ("#distFromSun").innerHtml = "Opps, Something went wrong,please try again"; //display a generic message to the user
    },
  });
  document.getElementById("detailsDiv").style.visibility = "visible";
});