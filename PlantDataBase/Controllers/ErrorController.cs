﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PlantDataBase.Controllers
{
    //controler for handling errors
    public class ErrorController : Controller
    {
        //Results are set to redirect to the error view by default
        public ActionResult Index()
        {
            return View("Error");
        }

        public ActionResult NotFound() //if we wanted to add differnt error pages for 404/500 errors, do it here. I choose to keep the errors generic
        {
            Response.StatusCode = 200;
            return View("Error");
        }

        public ActionResult InternalServer()
        {
            Response.StatusCode = 200;
            return View("Error");
        }
    }
}