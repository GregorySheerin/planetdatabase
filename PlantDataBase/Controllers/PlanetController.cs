﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PlantDataBase.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web.Http;

namespace PlantDataBase.Controllers
{
    //Api to handle calling for data about plants
    //use sealed to prevent inheritance(should never need to inherit this class)
    sealed public class PlanetController : ApiController
    {
        [HttpGet]//Restrict request to only Get requests
        [Route("api/Planet/Get")]  //Get all the planets in the Json
        public IEnumerable<string> Get() //return a enumerable obj of string,avoid exposing entire dataset
        {
            try //try and get the names from the json
            {
                return ReadJson().Select(x => x.Name); //Linq to return just the names of the planets
            }
            catch (Exception ex) //throw if expection
            {
                throw ex;
            }
                
        }

        [HttpGet]
        [Route("api/Planet/Get/{name:alpha}")] //Get the Distance from the sun of a particular planet
        public double Get(string name)
        {
            try //idenfical method to the above
            {
                return ReadJson().FirstOrDefault(x => x.Name == name).DistanceFromSun; //use linq in the same way
            }
            catch (Exception ex)
            {
                throw ex;
            }
             
            
        }

        
        //private method to read in the Json file, returns the list of planets as Planet objects
        //This way, I can avoid have the using block all over this class
        private List<PlanetModel> ReadJson()
        {
            //toDo : add some from of hashing/encrption, this isnt very "secure"
            using (StreamReader reader = new StreamReader(AppDomain.CurrentDomain.BaseDirectory + "/Data/PlanetData.json")) //read in the Json
            {
                return new List<PlanetModel>(JsonConvert.DeserializeObject<List<PlanetModel>>(reader.ReadToEnd())); //Convert it to a list of objects and return it
            }
        }
        
    }
}
