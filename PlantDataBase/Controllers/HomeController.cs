﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using PlantDataBase.Models;

namespace PlantDataBase.Controllers
{
    sealed public class HomeController : Controller
    {
        public ActionResult Index() //Result for the Home Page(Index.html)
        {    //When a user lands on the page, send an array of planets to the view  
            return View(GetPlanets());
        }

        public ActionResult DisplayDistanceFromSun(string name)
        {
            try //try and get the name and display the error
            {
                return PartialView("PlanetDetailView", GetPlanetDist(name));
            }
            catch (Exception ex) //if it fails, return a generic error to the user
            {

                return PartialView("Error"); //Returns a generic error
            }
           
        }


        //private function to get a array of planets
        private string[] GetPlanets()
        {
            using (var client = new HttpClient()) //using http client
            {
                //build up the Url for calling my api, this should work both localy and on a sever
                var getPlanetsUri = Url.RouteUrl(
                    "DefaultApi", //using the default api,set in webapi.config
                    new { httproute = "", controller = "Planet/Get", }, //we want to call the Get, without a param this will return the list of all planets
                    Request.Url.Scheme    //get the scheme, this will be localhost in my case, would be differnt if hosted 
                );

                //Get the plant names by calling the Api,using the URI built above
                return client //return an array of the planets
                            .GetAsync(getPlanetsUri) //get the URI
                            .Result //with the result
                            .Content.ReadAsAsync<string[]>().Result; ; //get its content and read it as a type(string array),finally get the result
                    

            }
        }

        private double GetPlanetDist(string name) //function to call 
        {
            using (var client = new HttpClient()) //using http client
            {

                var getPlanetDistUri = Url.RouteUrl(
                    "DefaultApi",
                    new { httproute = "", controller = "Planet/Get/" + name, }, //This time, attach the name onto the request, as specifed in the API
                    Request.Url.Scheme
                );

                return client.GetAsync(getPlanetDistUri).Result.Content.ReadAsAsync<double>().Result;//simlar to above, just with a double instead

            }
        }
    }
}