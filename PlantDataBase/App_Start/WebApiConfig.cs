﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace PlantDataBase
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            //My Api is fairly simple, little reason to change this
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{name}",
                defaults: new { name = RouteParameter.Optional } //changed to name, as not using Ids at the moment
            );
        }
    }
}
