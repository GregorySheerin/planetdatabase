﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PlantDataBase.Models
{
    //very simple model, only use if for deseliazing json at the moment
    sealed internal class PlanetModel
    {
        public string Name { get; set; }
        public double DistanceFromSun { get; set; }
    }
}